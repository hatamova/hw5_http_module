#!/usr/bin/env node

const readline = require('readline');
const input = readline.createInterface(process.stdin);
const myAPIKey = process.env.myAPIKey;
const http = require('http');


function getWeather(name) {
  const url = `http://api.weatherstack.com/current?access_key=${myAPIKey}&query=${name}`;

  const request = http.get(url, (response) => {
    const statusCode = response.statusCode;
  
    if (statusCode !== 200) {
      console.error(`Status Code: ${statusCode}`);
      return;
    }
  
    response.setEncoding('utf8')
  
    let rawData = '';
    response.on('data', (chunk) => rawData += chunk);
    response.on('end', () => {
      const data = JSON.parse(rawData);
      console.log(`In ${data.location.name} ${data.location.localtime} weather is ${data.current.weather_descriptions},
        temperature: ${data.current.wind_speed},
        wind: ${data.current.wind_speed}`
      );
      input.close();
    })
  })
  
  request.on('error', (e) => {
    console.error(`Got error: ${e.message}`);
  })
};

console.log('Get weather in ...?');
input.on('line', (name) => getWeather(name));